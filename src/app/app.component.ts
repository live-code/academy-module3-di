import { Component } from '@angular/core';

@Component({
  selector: 'ac-root',
  template: `
    <ac-nav-bar></ac-nav-bar>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {

}
