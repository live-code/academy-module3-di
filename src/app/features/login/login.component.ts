import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';
import { Auth } from '../../model/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'ac-login',
  template: `
    <div class="alert alert-danger" *ngIf="authService.error"> ahia!</div>
    <form #f="ngForm" (submit)="login(f.value)">
      <input type="text" ngModel name="user">
      <input type="text" ngModel name="pass">
      <button type="submit">LOGIN</button>
    </form>
  `,
})
export class LoginComponent {
  constructor(
    private router: Router,
    public authService: AuthService
  ) {}

  login(data: any) {
    this.authService.login(data.user, data.pass)
      .then(res => {
        this.router.navigateByUrl('home')
      })
      .catch(err => {
        console.log('err', err.status)
      })
  }
}
