
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Product } from '../../../model/product';

@Component({
  selector: 'ac-catalog-list',
  template: `
    <div class="d-flex">
      <div
        class="card m-2"
        *ngFor="let product of products"
      >
        <div class="card-header">{{product.name}}</div>
        <div class="card-body">
          Prezzo: € {{product.cost}}
          <hr>
          <button
            class="btn btn-outline-primary"
            (click)="addToCart.emit(product)">
            <i class="fa fa-cart-plus"></i>
          </button>
        </div>

      </div>
    </div>
  `,
})
export class CatalogListComponent  {
 @Input() products: Product[] = [];
 @Output() addToCart = new EventEmitter<Product>()

}
