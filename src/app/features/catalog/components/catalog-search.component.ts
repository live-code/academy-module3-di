import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'ac-catalog-search',
  template: `
    <div>
      <h3>Search {{text}}</h3>
      <input
        type="text" class="form-control m-2" placeholder="search"
        #inputSearch
        [ngModel]="text"
        (keyup.enter)="search.emit(inputSearch.value)"
      >
    </div>
  `,
})
export class CatalogSearchComponent  {
  @Input() text: string = '';
  @Output() search = new EventEmitter<string>()
}
