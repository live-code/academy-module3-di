import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';
import { CatalogSearchComponent } from './components/catalog-search.component';
import { CatalogListComponent } from './components/catalog-list.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CatalogComponent,
    CatalogSearchComponent,
    CatalogListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    CatalogRoutingModule
  ]
})
export class CatalogModule { }
