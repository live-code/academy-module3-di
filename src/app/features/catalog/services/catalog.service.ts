import { Injectable } from '@angular/core';
import { Product } from '../../../model/product';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {
  products: Product[] = [];
  searchedText = '';

  constructor(private http: HttpClient){}

  doSearch() {
    if (this.searchedText) {
      this.http.get<Product[]>('http://localhost:3000/catalog?q=' + this.searchedText)
        .subscribe(res => this.products = res)
    }
    else {
      this.getAll();
    }
  }

  getAll() {
    this.http.get<Product[]>('http://localhost:3000/catalog')
      .subscribe(res => this.products = res)
  }

  search(value: string) {
    this.searchedText = value;
    this.doSearch();
  }


}
