import { Component } from '@angular/core';
import { CartService } from '../../core/services/cart.service';
import { CatalogService } from './services/catalog.service';
import { LogService } from '../../core/services/log.service';

@Component({
  selector: 'ac-catalog',
  template: `
   <ac-catalog-search 
     [text]="catalogService.searchedText"
     (search)="catalogService.search($event)"></ac-catalog-search>
   
   <ac-catalog-list
     [products]="catalogService.products"
     (addToCart)="cartService.addToCart($event)"
   ></ac-catalog-list>
  `,
})
export class CatalogComponent {
  constructor(
    public cartService: CartService,
    public catalogService: CatalogService,
    private logService: LogService
  ) {
    this.catalogService.doSearch()
    logService.log();
  }

}
