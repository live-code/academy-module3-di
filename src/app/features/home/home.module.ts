import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { ThemeService } from '../../core/services/theme.service';
import { SharedModule } from '../../shared/shared.module';
import { MapService } from '../../shared/map.service';


@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule
  ],
  providers: [
  ],
})
export class HomeModule { }
