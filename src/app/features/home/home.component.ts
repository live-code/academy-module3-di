import { Component, OnInit, Optional } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';
import { MapService } from '../../shared/map.service';
import { CounterService } from '../../shared/counter.service';

@Component({
  selector: 'ac-home',
  template: `
    <button (click)="counterService.inc()">
      home works! {{counterService.value}}
    </button>
    
    <ac-map config="XYZ"></ac-map>
    <ac-map config="ABC"></ac-map>
  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit {
  constructor(@Optional() mapService: MapService, public counterService: CounterService) {
  }
  ngOnInit(): void {
  }

}
