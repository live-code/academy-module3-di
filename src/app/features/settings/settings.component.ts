import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';
import { CounterService } from '../../shared/counter.service';

@Component({
  selector: 'ac-settings',
  template: `
    <h1>Settings {{themeService.theme}}</h1>
    <button (click)="themeService.theme = 'dark'">Set Dark</button>
    <button (click)="themeService.theme = 'light'">Set light</button>

    <button (click)="counterService.inc()">
      home works! {{counterService.value}}
    </button>

  `,
})
export class SettingsComponent implements OnInit {

  constructor(
    public counterService: CounterService,
    public themeService: ThemeService) { }

  ngOnInit(): void {
  }

}
