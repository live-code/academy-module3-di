import { Component, Inject, OnInit } from '@angular/core';
import { CartService } from '../../core/services/cart.service';
import { LogService } from '../../core/services/log.service';
import { environment } from '../../../environments/environment';
import { CONFIG } from '../../core/services/config';

@Component({
  selector: 'ac-cart',
  template: `
    <li *ngFor="let item of cartService.items">
      {{item.product.name}} € {{item.product.cost}}  
      <button (click)="cartService.dec(item.product.id)">-</button>
        {{item.qty}}
      <button (click)="cartService.inc(item.product.id)">+</button>
     
      <button (click)="cartService.remoteFromCart(item.product.id)">
        Remove 
      </button>
    </li>
    
    <hr>
    Total: € {{cartService.getTotalCost()}}
  `,
})
export class CartComponent {
  constructor(
    public cartService: CartService,
    private logService: LogService,
    @Inject(CONFIG) config: number
  ) {
    logService.log()
    console.log(config)

  }
}
