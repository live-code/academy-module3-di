import { Component, Input, OnInit } from '@angular/core';
import { MapService } from './map.service';
import { map } from 'rxjs';

@Component({
  selector: 'ac-map',
  template: `
    <p>
      map works!
    </p>
    {{mapService.value}}
  `,
  providers: [
    MapService
  ]
})
export class MapComponent implements OnInit {
  @Input() config: string = '';

  constructor(public mapService: MapService) { }

  ngOnInit(): void {
    this.mapService.value = this.config;
  }

}
