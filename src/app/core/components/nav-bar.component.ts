import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../services/theme.service';
import { CartService } from '../services/cart.service';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ac-nav-bar',
  template: `
    <div
      class="navbar" 
      [ngClass]="{
        lightMode: themeService.theme === 'light', 
        darkMode: themeService.theme === 'dark'
      }"
    >
      <button routerLink="login" routerLinkActive="active">login</button>
      <button
        *ngIf="authService.isLogged()"
        routerLink="home" routerLinkActive="active">home</button>
      <button routerLink="cart" routerLinkActive="active">cart</button>
      <button routerLink="settings" routerLinkActive="active">settings</button>
      <button routerLink="catalog" routerLinkActive="active">catalog</button>
      <button
        *ngIf="authService.isLogged()"
        (click)="logoutHandler()">logout</button>
      
      <div style="color: white">
        TOTAL: {{cartService.getTotal()}}
        <span *ngIf="authService.auth"> - {{authService.auth.displayName}}</span>
      </div>
      
    </div>
  `,
  styles: [`
    .navbar {padding: 20px}
    .darkMode { background-color: black }
    .lightMode { background-color: gray }
    .active { background-color: orange}
  `]
})
export class NavBarComponent {

  constructor(
    public themeService: ThemeService,
    public cartService: CartService,
    public authService: AuthService,
    private router: Router
  ) {}

  logoutHandler() {
    this.authService.logout();
    this.router.navigateByUrl('login')
  }
}
