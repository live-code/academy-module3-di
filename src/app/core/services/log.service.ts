import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from './config';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(
    http: HttpClient,
    @Inject(CONFIG) private config: number
  ) {
    console.log('log service')
  }
  init() {
    console.log('init log')
  }

  log() {
    console.warn('bla bla', this.config)
  }
}
