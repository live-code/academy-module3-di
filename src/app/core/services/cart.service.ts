import { Injectable } from '@angular/core';
import { Product } from '../../model/product';

export interface CartItem {
  product: Product,
  qty: number
}
@Injectable({
  providedIn: 'root'
})
export class CartService {
  items: CartItem[] = [];

  addToCart(product: Product) {
    const index = this.items.findIndex(item => item.product.id === product.id)
    if (index !== -1) {
      this.items[index].qty++;
    } else {
      this.items.push({ product: product, qty: 1 })
    }
  }

  remoteFromCart(id: number) {
    const index = this.items.findIndex(item => item.product.id === id);
    this.items.splice(index, 1)
  }

  inc(id: number) {
    const index = this.items.findIndex(item => item.product.id === id);
    this.items[index].qty++;
  }

  dec(id: number) {
    const index = this.items.findIndex(item => item.product.id === id);
    if (this.items[index].qty > 1) {
      this.items[index].qty--;
    }
  }

  getTotal() {
    let total = 0;
    this.items.forEach(item => {
      total += item.qty
    })
    return total;
  }

  getTotalCost() {
    return this.items.reduce((acc, item) =>
      acc + item.product.cost * item.qty, 0)
  }
}
