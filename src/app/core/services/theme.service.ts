import { Injectable } from '@angular/core';

export type Theme = 'dark' | 'light';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private value: Theme = 'dark';

  constructor() {
    this.value = localStorage.getItem('theme') as Theme || 'dark';
  }

  set theme(theme: Theme) {
    localStorage.setItem('theme', theme)
    this.value = theme;
  }

  get theme(): Theme {
    return this.value
  }

}
