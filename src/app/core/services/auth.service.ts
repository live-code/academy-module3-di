import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Auth } from '../../model/auth';
import { share } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth: Auth | null = null
  error = false;

  constructor(
    private http: HttpClient,
  ) {
    const isAuth = localStorage.getItem('auth');
    if (isAuth) {
      this.auth = JSON.parse(isAuth)
    }
  }

  login(username: string, password: string) {
    this.error = false;
    return new Promise((resolve, reject) => {
      this.http.get<Auth>('http://localhost:3000/login')
        .subscribe(
          res => {
            this.auth = res;
            localStorage.setItem('auth', JSON.stringify(res))
            resolve(res);
          },
          err => {
            this.error = true
            reject(err)
          }
        )
    })
  }

  logout() {
    this.auth = null;
    localStorage.removeItem('auth');
  }

  isLogged(): boolean {
    return !!this.auth
  }
}
