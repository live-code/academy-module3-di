import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from './config';

@Injectable({
  providedIn: 'root'
})
export class FakeLogService {

  constructor(
    http: HttpClient,
    @Inject(CONFIG) private config: number
    ) {
    console.log('fake log service')
  }
  init() {
    console.log('fake init log')
  }

  log() {
    console.warn('fake bla bla', this.config)
  }
}
