import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { catchError, Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor{
  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let cloned = req;
    if (this.authService.isLogged() && req.url.includes('catalog')) {
      cloned = req.clone({
        setHeaders: {
          Authorization: 'Bearer ' + this.authService.auth?.token
        }
      })
    }
    return next.handle(cloned)
  }

}
