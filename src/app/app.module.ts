import { InjectionToken, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { ThemeService } from './core/services/theme.service';
import { NavBarComponent } from './core/components/nav-bar.component';
import { AuthService } from './core/services/auth.service';
import { AuthInterceptor } from './core/services/auth.interceptor';
import { LogService } from './core/services/log.service';
import { FakeLogService } from './core/services/fakelog.service';
import { environment } from '../environments/environment';
import { CONFIG } from './core/services/config';
import { SharedModule } from './shared/shared.module';


@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule.forRoot()
  ],
  providers: [
    // AuthService,
    // { provide: AuthService, useClass: AuthService},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: CONFIG,
      useValue: 567
    },
    {
      provide: LogService,
      useFactory: (http: HttpClient, config: number) => {
        return environment.production ?
          new LogService(http, config) :
          new FakeLogService(http, config)
      },
      deps: [HttpClient, CONFIG]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private logService: LogService) {
    logService.init();
  }
}
